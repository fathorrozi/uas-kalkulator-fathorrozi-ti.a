// CurrencyExchangeScreen.js
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const CurrencyExchangeScreen = ({ setShowCurrencyExchange }) => {
  const handleBackButtonPress = () => {
    setShowCurrencyExchange(false);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Currency Exchange</Text>
      <Text>This is the currency exchange screen!</Text>
      <TouchableOpacity style={styles.backButton} onPress={handleBackButtonPress}>
        <Text style={styles.backButtonText}>Back</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  backButton: {
    marginTop: 20,
    padding: 10,
    backgroundColor: '#007BFF',
    borderRadius: 5,
  },
  backButtonText: {
    color: '#fff',
    fontSize: 16,
  },
});

export default CurrencyExchangeScreen;
