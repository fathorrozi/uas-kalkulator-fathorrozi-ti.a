import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const CalculatorApp = () => {
  const [display, setDisplay] = useState('0');

  const handleButtonPress = (value) => {
    if (value === 'C') {
      setDisplay('0');
    } else if (value === '=') {
      try {
        setDisplay(eval(display).toString());
      } catch (error) {
        setDisplay('Input Yang Bener!');
      }
    } else {
      setDisplay(display === '0' ? value : display + value);
    }
  };

  const renderButtons = () => {
    const buttons = [
      '7', '8', '9', '/',
      '4', '5', '6', '*',
      '1', '2', '3', '-',
      'C', '0', '=', '+'
    ];

    return buttons.map((buttonValue) => (
      <TouchableOpacity
        key={buttonValue}
        style={styles.button}
        onPress={() => handleButtonPress(buttonValue)}
      >
        <Text style={styles.buttonText}>{buttonValue}</Text>
      </TouchableOpacity>
    ));
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
      <Text style={styles.headerText}>Kalkulator</Text>
        <TouchableOpacity style={styles.headerButton}>
          <Text style={styles.alignItems}>Nilai Tukar</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.display}>{display}</Text>
      <View style={styles.buttonsContainer}>
        {renderButtons()}
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Fathor.Rozi - TI.A © 2023</Text>
        
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  display: {
    fontSize: 48,
    marginBottom: 20,
    textAlign: 'right',
    padding: 110,
  },
  buttonsContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '25%',
    height: 100,
    borderWidth: 1,
    borderColor: '#ccc',
  },
  buttonText: {
    fontSize: 24,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderBottomWidth: 0,
    borderBottomColor: '',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  headerButton: {
    padding: 8,
  },
  headerButtonText: {
    fontSize: 16,
    color: '#007BFF',
  },
});

export default CalculatorApp;
